/*
package com.example.android.crimealert;


import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Random;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

import static android.support.constraint.Constraints.TAG;


*/
/**
 * A simple {@link Fragment} subclass.
 *//*

public class ReportFragment extends Fragment implements OnMapReadyCallback,
      GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnMapClickListener,
        GoogleMap.OnMarkerClickListener,
        LocationListener,
        View.OnClickListener {

    SupportMapFragment mapFragment;
    GoogleMap mGoogleMap;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrent;

    private static final int MY_PERMISSION_REQUEST_CODE = 2299;
    private static final int PLAY_SERVICE_RESOLUTION_REQUEST = 300;
    private static int UPDATE_INTERVAL = 5000;
    private static int FASTEST_INTERVAL = 3000;
    private static int DISPLACEMENT = 10;

    EditText etChooseDate;
    EditText etChooseTime;
    EditText editTextDesc;
    Button buttonReport;
    String value_lat = null;
    String value_lng = null;
    FirebaseAuth auth;
    Double lat1;
    Double lat2;


    //UI References
    private EditText getDateEtxt;
    private EditText getTimeEtxt;


    private DatePickerDialog fromDatePickerDialog;
    private TimePickerDialog fromTimePickerDialog;

    private SimpleDateFormat dateFormatter;
    private SimpleTimeZone timeFormatter;

    DatabaseReference databaseCrimes;
    GeoFire geoFire;

    public ReportFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_report, container, false);

        auth = FirebaseAuth.getInstance();
        databaseCrimes = FirebaseDatabase.getInstance().getReference("crimes");
        geoFire = new GeoFire(databaseCrimes);

        if(mapFragment == null){
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction ft = fragmentManager.beginTransaction();
            mapFragment = SupportMapFragment.newInstance();
            ft.replace(R.id.map, mapFragment).commit();
        }
        mapFragment.getMapAsync(this);

        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        timeFormatter = new SimpleTimeZone(3,String.valueOf(TimeZone.getDefault()));

        editTextDesc = (EditText) v.findViewById(R.id.editTextDesc);
        etChooseDate = (EditText) v.findViewById(R.id.etChooseDate);
        etChooseTime = (EditText) v.findViewById(R.id.etChooseTime);
        buttonReport = (Button) v.findViewById(R.id.buttonReport);


        buttonReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addReport();

            }
        });

        setUpLocation();
        findViewsByID(v);

        setDateTimeField();


        return v;
        }

    private void setUpLocation() {

        if(ActivityCompat.checkSelfPermission(getActivity(),Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(),Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(getActivity(),new String[]{
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
        }, MY_PERMISSION_REQUEST_CODE);
        }else
        {
            if(checkPlayServices()) {

                buildGoogleApiClient();
                createLocationRequest();
                displayLocation();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode)
        {
            case MY_PERMISSION_REQUEST_CODE:
                if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                        if(checkPlayServices()) {
                            buildGoogleApiClient();
                            createLocationRequest();
                            displayLocation();
                        }
                }
                break;
        }
    }

    private void displayLocation() {
        if(ActivityCompat.checkSelfPermission(getActivity(),Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(),Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if(mLastLocation != null){
            double latitude = mLastLocation.getLatitude();
            double longitude = mLastLocation.getLongitude();
            */
/*value_lat = String.valueOf(latitude);
            value_lng = String.valueOf(longitude);*//*


            //Updates to firebase
            geoFire.setLocation("You", new GeoLocation(latitude,longitude),
            new GeoFire.CompletionListener() {
                @Override
                public void onComplete(String key, DatabaseError error) {
                    //Add marker
                    if(mCurrent != null)
                        mCurrent.remove(); //remove old marker

                    LatLng latLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(latLng);
                    markerOptions.title("Current Position");
                    markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
                    mCurrent = mGoogleMap.addMarker(markerOptions);
                    */
/*mCurrent = mGoogleMap.addMarker(new MarkerOptions()
                    .position(new LatLng(mLastLocation.getLatitude(),mLastLocation.getLongitude()))
                    .title("You"));*//*


                    //move Camera to this position
                    mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16));
                }
            });



            Log.d("Crime App", String.format("Your location was changed : %f / %f",latitude,longitude ));
        }
        else
        {
            Log.d("Crime App", "Can not get your location");
        }
    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        mLocationRequest.setSmallestDisplacement(DISPLACEMENT);
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
        .addConnectionCallbacks(this)
        .addOnConnectionFailedListener(this)
        .addApi(LocationServices.API)
        .build(); {
        }
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode))
                GooglePlayServicesUtil.getErrorDialog(resultCode, getActivity(), PLAY_SERVICE_RESOLUTION_REQUEST).show();

            else {
                Toast.makeText(getActivity(), "This device is not supported", Toast.LENGTH_SHORT).show();
                getActivity().finish();
            }
            return false;
        }
        return true;
    }


    private void setDateTimeField() {
            getDateEtxt.setOnClickListener(this);
            getTimeEtxt.setOnClickListener(this);

        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(getContext(), new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                getDateEtxt.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        fromTimePickerDialog = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                Calendar newTime = Calendar.getInstance();
//                hourOfDay = newTime.get(Calendar.HOUR);
//                minute = newTime.get(Calendar.MINUTE);


                getTimeEtxt.setText(hourOfDay + ":" + String.format("%02d", minute));
            }
        }, newCalendar.get(Calendar.HOUR), newCalendar.get(Calendar.MINUTE),false);
    }

    private void findViewsByID(View v) {
        getDateEtxt = (EditText) v.findViewById(R.id.etChooseDate);
        getDateEtxt.setInputType(InputType.TYPE_NULL);
        getDateEtxt.requestFocus();

        getTimeEtxt = (EditText) v.findViewById(R.id.etChooseTime);
        getTimeEtxt.setInputType(InputType.TYPE_NULL);
        getTimeEtxt.requestFocus();
    }

    @Override
    public void onClick(View view) {
        if(view == getDateEtxt) {
            fromDatePickerDialog.show();
        }
        if(view == getTimeEtxt){
            fromTimePickerDialog.show();
        }
    }



    private void addReport(){
        String the_date = etChooseDate.getText().toString().trim();
        String the_time = etChooseTime.getText().toString().trim();
        String desc = editTextDesc.getText().toString().trim();
        String value_lat= String.valueOf(mLastLocation.getLatitude());
        String value_lng =String.valueOf(mLastLocation.getLongitude());
        FirebaseUser user = auth.getCurrentUser();
        String username = user.getDisplayName();

        if(!TextUtils.isEmpty(desc)  && !TextUtils.isEmpty(the_date) && !TextUtils.isEmpty(the_time)){
            String id = databaseCrimes.push().getKey();

            Crimes crime = new Crimes(id,the_date,the_time,desc,value_lat,value_lng,username);
            databaseCrimes.child(id).setValue(crime);
            Toast.makeText(getContext(),"Crime recorded", Toast.LENGTH_LONG).show();
            etChooseDate.setText(null);
            etChooseTime.setText(null);
            editTextDesc.setText(null);
        }else{

        }
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        displayLocation();
        startLocationUpdates();
    }

    public void startLocationUpdates() {
        if(ActivityCompat.checkSelfPermission(getActivity(),Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(),Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,mLocationRequest,this);

    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mLastLocation = location;
        displayLocation();

    }

    @Override
    public void onMapClick(LatLng latLng) {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

    */
/*    databaseCrimes = FirebaseDatabase.getInstance().getReference().child("crimes");
        databaseCrimes.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot ds : dataSnapshot.getChildren()) {
                    //Specify your model class here
                    Crimes crimes = new Crimes();
                    crimes.setCrimeLat(ds.getValue(Crimes.class).getCrimeLat());
                    crimes.setCrimeLong(ds.getValue(Crimes.class).getCrimeLong());
                    String latitude1 = crimes.getCrimeLat();
                    String latitude2 = crimes.getCrimeLong();
                    lat1 = Double.parseDouble(latitude1);
                    lat2 = Double.parseDouble(latitude2);

            }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }

        });*//*

        LatLng dangerous_area = new LatLng(35.7533,-122.4056);
        mGoogleMap.addCircle(new CircleOptions()
        .center(dangerous_area)
        .radius(500)
        .strokeColor(Color.BLUE)
        .fillColor(0x220000FF)
        .strokeWidth(5.0f));

        GeoQuery geoQuery = geoFire.queryAtLocation(new GeoLocation(dangerous_area.latitude,dangerous_area.longitude), 0.5f);
        geoQuery.addGeoQueryEventListener(new GeoQueryEventListener() {
            @Override
            public void onKeyEntered(String key, GeoLocation location) {
                sendNotification("Crime Alert!!", String.format("%s are entering a dangerous area",key));
            }

            @Override
            public void onKeyExited(String key) {
                sendNotification("Crime Alert!!", String.format("%s are leaving the dangerous area",key));

            }

            @Override
            public void onKeyMoved(String key, GeoLocation location) {
                Log.d("MOVE", String.format("@s moved within the dangerous area [%f/%f]", key,location.latitude,location.longitude));
            }

            @Override
            public void onGeoQueryReady() {

            }

            @Override
            public void onGeoQueryError(DatabaseError error) {
                Log.e("Error", ""+error);

            }
        });

    }
    private void sendNotification(String title, String content) {
        Notification.Builder builder = new Notification.Builder(getActivity())
                .setSmallIcon(R.drawable.ic_siren)
                .setContentTitle(title)
                .setContentText(content);
        NotificationManager manager = (NotificationManager)getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
        Intent intent = new Intent(getActivity(), ReportFragment.class);
        PendingIntent contentIntent = PendingIntent.getActivity(getActivity(),0,intent,PendingIntent.FLAG_IMMUTABLE);
        builder.setContentIntent(contentIntent);
        Notification notification = builder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notification.defaults |= Notification.DEFAULT_SOUND;

        manager.notify(new Random().nextInt(),notification);
    }
}


*/
