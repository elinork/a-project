package com.example.android.crimealert;

public class Crimes {
    String crimeId;
    String crimeDate;
    String crimeTime;
    String crimeDesc;
    String crimeType;
    /*String crimeLat;
    String crimeLong*/;
    String username;

    public Crimes() {

    }

    public Crimes(String crimeId, String crimeDate, String crimeTime, String crimeDesc, String crimeType /*String crimeLat, String crimeLong*/, String username) {
        this.crimeId = crimeId;
        this.crimeDate = crimeDate;
        this.crimeTime = crimeTime;
        this.crimeDesc = crimeDesc;
        /*this.crimeLat = crimeLat;
        this.crimeLong = crimeLong;*/
        this.crimeType = crimeType;
        this.username = username;
    }


    public String getCrimeId() {
        return crimeId;
    }

    public String getCrimeDate() {
        return crimeDate;
    }

    public String getCrimeTime() {
        return crimeTime;
    }

    public String getCrimeDesc() {
        return crimeDesc;
    }

    public String getCrimeType(){ return crimeType; }

    /*public String getCrimeLat() {
        return crimeLat;
    }

    public String getCrimeLong() {
        return crimeLong;
    }*/

    public String getUsername() { return username; }

    public void setCrimeId(String crimeId) {
        this.crimeId = crimeId;
    }

    public void setCrimeDate(String crimeDate) {
        this.crimeDate = crimeDate;
    }

    public void setCrimeTime(String crimeTime) {
        this.crimeTime = crimeTime;
    }

    public void setCrimeDesc(String crimeDesc) {
        this.crimeDesc = crimeDesc;
    }

    public void setCrimeType(String crimeType) { this.crimeType = crimeType;}

  /*  public void setCrimeLat(String crimeLat) {
        this.crimeLat = crimeLat;
    }

    public void setCrimeLong(String crimeLong) {
        this.crimeLong = crimeLong;
    }*/

    public void setUsername(String username) {
        this.username = username;
    }
}