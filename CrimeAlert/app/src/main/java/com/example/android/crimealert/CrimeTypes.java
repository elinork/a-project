package com.example.android.crimealert;

public class CrimeTypes {
    String crimeType;

    public CrimeTypes(){}

    public CrimeTypes(String crimeType) {
        this.crimeType = crimeType;
    }

    public String getCrimeType() {
        return crimeType;
    }
}
