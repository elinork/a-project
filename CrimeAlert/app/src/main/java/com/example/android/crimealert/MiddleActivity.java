package com.example.android.crimealert;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MiddleActivity extends AppCompatActivity {

    Button buttonReport;
    Button buttonTrends;
    TextView textTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_middle);

        buttonReport = (Button) findViewById(R.id.buttonReport);
        buttonTrends = (Button) findViewById(R.id.buttonTrends);
        textTitle = (TextView) findViewById(R.id.textViewTitle);
        textTitle.setText(getIntent().getStringExtra("VALUE"));

        buttonReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(android.view.View view) {
                String value = textTitle.getText().toString();
                Intent intent = new Intent(MiddleActivity.this, ReportCrimeActivity.class);
                intent.putExtra("ANOTHER", value);
                startActivity(intent);
            }});
        buttonTrends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = textTitle.getText().toString();
                Intent intent  = new Intent(MiddleActivity.this, CrimePostsActivity.class);
                intent.putExtra("ANOTHER", value);
                startActivity(intent);
            }
        });



    }
}
