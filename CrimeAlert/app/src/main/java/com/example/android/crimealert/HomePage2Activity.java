package com.example.android.crimealert;

import android.content.Intent;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.concurrent.Executor;

public class HomePage2Activity extends AppCompatActivity {

    ImageButton assault,accident,wanted,theft,kidnap,fire ;
    TextView textAssault, textRobbery, textAccident, textWanted,textKidnap, textFire;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page2);

        assault = (ImageButton) findViewById(R.id.imageButtonAssault);
        accident = (ImageButton) findViewById(R.id.imageButtonAccident);
        wanted = (ImageButton) findViewById(R.id.imageButtonWanted);
        theft = (ImageButton) findViewById(R.id.imageButtonRobbery);
        kidnap = (ImageButton) findViewById(R.id.imageButtonKidnap);
        fire = (ImageButton) findViewById(R.id.imageButtonFire);
        textAssault = (TextView) findViewById(R.id.textAssault);
        textRobbery = (TextView) findViewById(R.id.textRobbery);
        textAccident = (TextView) findViewById(R.id.textAccident);
        textWanted = (TextView) findViewById(R.id.textWanted);
        textKidnap = (TextView) findViewById(R.id.textKidnap);
        textFire = (TextView) findViewById(R.id.textFire);


       assault.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

               String value = textAssault.getText().toString();
               Intent intent = new Intent(HomePage2Activity.this, ReportCrimeActivity.class);
               intent.putExtra("VALUE", value);
               startActivity(intent);
           }});
       accident.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String value = textAccident.getText().toString();
                Intent intent = new Intent(HomePage2Activity.this, ReportCrimeActivity.class);
                intent.putExtra("VALUE", value);
                startActivity(intent);
            }});
        wanted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String value = textWanted.getText().toString();
                Intent intent = new Intent(HomePage2Activity.this, ReportCrimeActivity.class);
                intent.putExtra("VALUE", value);
                startActivity(intent);
            }});
        theft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String value = textRobbery.getText().toString();
                Intent intent = new Intent(HomePage2Activity.this, ReportCrimeActivity.class);
                intent.putExtra("VALUE", value);
                startActivity(intent);
            }});
        kidnap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String value = textKidnap.getText().toString();
                Intent intent = new Intent(HomePage2Activity.this, ReportCrimeActivity.class);
                intent.putExtra("VALUE", value);
                startActivity(intent);
            }});
        fire.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String value = textFire.getText().toString();
                Intent intent = new Intent(HomePage2Activity.this, ReportCrimeActivity.class);
                intent.putExtra("VALUE", value);
                startActivity(intent);
            }});

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_page, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_signOut){
            AuthUI.getInstance()
                    .signOut(this)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        public void onComplete(@NonNull Task<Void> task) {
                            // user is now signed out
                            startActivity(new Intent(HomePage2Activity.this, MainActivity.class));
                            finish();
                        }
                    });

        }if(item.getItemId() == R.id.action_view_posts){
            Intent intent = new Intent(HomePage2Activity.this , CrimePostsActivity.class);
            startActivity(intent);
        }

        else {
            return super.onOptionsItemSelected(item);
        }
        return true;
    }



}
