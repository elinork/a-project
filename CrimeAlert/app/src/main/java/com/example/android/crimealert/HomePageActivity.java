/*
package com.example.android.crimealert;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;


import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

public class HomePageActivity extends AppCompatActivity {
    private ActionBar toolbar;
    FirebaseAuth auth;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        auth = FirebaseAuth.getInstance();
//        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
//            NotificationChannel channel = new NotificationChannel(ReportFragment.CHANNEL_ID,ReportFragment.CHANNEL_NAME,NotificationManager.IMPORTANCE_DEFAULT);
//            channel.setDescription(ReportFragment.CHANNEL_DESC);
//            NotificationManager notificationManager = getSystemService(NotificationManager.class);
//            notificationManager.createNotificationChannel(channel);
//
//        }
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if(task.isSuccessful()){
                            String token = task.getResult().getToken();
                            saveToken(token);
                        }
                    }
                });

        toolbar = getSupportActionBar();



        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        toolbar.setTitle("View Trending");
        loadFragment(new TrendingFragment());

    }
    private void saveToken(String token){
        String email = auth.getCurrentUser().getEmail();
        Users users = new Users(email,token);
        DatabaseReference dbUsers = FirebaseDatabase.getInstance().getReference("users");
        dbUsers.child(auth.getCurrentUser().getUid())
                .setValue(users);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home_page, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_signOut){
            AuthUI.getInstance()
                    .signOut(this)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        public void onComplete(@NonNull Task<Void> task) {
                            // user is now signed out
                            startActivity(new Intent(HomePageActivity.this, MainActivity.class));
                            finish();
                        }
                    });

        }else {
            return super.onOptionsItemSelected(item);
        }
        return true;
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.navigation_report:
                    toolbar.setTitle("Report Crime");
                    fragment = new AnalyticsFragment();
                    loadFragment(fragment);
                    return true;
                case R.id.navigation_trends:
                    toolbar.setTitle("View Trending");
                    fragment = new TrendingFragment();
                    loadFragment(fragment);
                    return true;
                case R.id.navigation_analytics:
                    toolbar.setTitle("View Analytics");
                    fragment = new AnalyticsFragment();
                    loadFragment(fragment);
                    return true;
//                case R.id.navigation_profile:
//                    toolbar.setTitle("Profile");
//                    fragment = new ProfileFragment();
//                    loadFragment(fragment);
//                    return true;
            }
            return false;
        }

    };

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
*/







































//  auth = FirebaseAuth.getInstance();
//        user = auth.getCurrentUser();
//
//        PD = new ProgressDialog(this);
//        PD.setMessage("Loading...");
//        PD.setCancelable(true);
//        PD.setCanceledOnTouchOutside(false);
//
//        btnSignOut = (Button) findViewById(R.id.button 2);
//
//        btnSignOut.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                auth.signOut();
//                FirebaseAuth.AuthStateListener authListener = new FirebaseAuth.AuthStateListener() {
//                    @Override
//                    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
//                        FirebaseUser user = firebaseAuth.getCurrentUser();
//                        if (user == null) {
//                            startActivity(new Intent(HomePageActivity.this, MainActivity.class));
//                            finish();
//                        }else{
//                            Toast.makeText(HomePageActivity.this, "Unable to load", Toast.LENGTH_SHORT).show();
//                        }
//                    }
//                };
//            }
//        });