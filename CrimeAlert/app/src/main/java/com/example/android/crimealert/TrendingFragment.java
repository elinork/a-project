/*
package com.example.android.crimealert;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


*/
/**
 * A simple {@link Fragment} subclass.
 *//*

public class TrendingFragment extends Fragment {

    ListView trendingListView;
    DatabaseReference databaseCrimes;
    List<Crimes> crimesList;


    public TrendingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_trending, container, false);
        databaseCrimes = FirebaseDatabase.getInstance().getReference("crimes");

        trendingListView = (ListView) v.findViewById(R.id.trendingListView);
        crimesList = new ArrayList<>();


        return v;

    }

    @Override
    public void onStart() {
        super.onStart();

         databaseCrimes.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot crimeSnapshot : dataSnapshot.getChildren()){
                    Crimes crimes = crimeSnapshot.getValue(Crimes.class);
                    crimesList.add(crimes);
                }

                CrimeList adapter = new CrimeList(getActivity(),crimesList);
                trendingListView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
*/
