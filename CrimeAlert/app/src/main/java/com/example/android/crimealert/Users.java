package com.example.android.crimealert;

public class Users {

    public String email;
    public String token;

    public Users(String email, String token) {
        this.email = email;
        this.token = token;
    }
}
