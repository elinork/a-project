package com.example.android.crimealert;

import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class NotificationActivity extends AppCompatActivity {

    private static final String CHANNEL_ID = "Crime_alert";
    private static final String CHANNEL_NAME= "Crime reports";
    private static final String CHANNEL_DESC = "Crime report notifications";
    DatabaseReference databaseCrimes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        databaseCrimes = FirebaseDatabase.getInstance().getReference("crimes");
        setContentView(R.layout.activity_notification);
    }

    private void displayNotification(){
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this,CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_siren)
                .setContentTitle("Hurray it is working")
                .setContentText("Your first notification. . .")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        NotificationManagerCompat nmgr = NotificationManagerCompat.from(this);
        nmgr.notify(1,mBuilder.build());

    }
}
