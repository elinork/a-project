package com.example.android.crimealert;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class CrimeList extends ArrayAdapter<Crimes> {
    private Activity context;
    private List<Crimes> crimesList;

    public CrimeList(Activity context, List<Crimes> crimesList){
        super(context,R.layout.list_layout,crimesList);
        this.context = context;
        this.crimesList = crimesList;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.list_layout, null,true);

        TextView textViewUsername = (TextView) listViewItem.findViewById(R.id.textViewUsername);
        TextView textViewDesc = (TextView) listViewItem.findViewById(R.id.textViewDesc);
        TextView textViewDate = (TextView) listViewItem.findViewById(R.id.textViewDate);
        TextView textViewTime = (TextView) listViewItem.findViewById(R.id.textViewTime);

        Crimes crimes = crimesList.get(position);

        textViewUsername.setText(crimes.getUsername());
        textViewDesc.setText(crimes.getCrimeDesc());
        textViewDate.setText(crimes.getCrimeDate());
        textViewTime.setText(crimes.getCrimeTime());

        return listViewItem;
    }
}
