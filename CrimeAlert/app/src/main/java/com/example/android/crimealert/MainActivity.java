package com.example.android.crimealert;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity {

  private EditText inputEmail, inputPassword;
  private FirebaseAuth auth;
  private Button btnSignUp, btnLogin;
  private ProgressDialog PD;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    this.requestWindowFeature(Window.FEATURE_NO_TITLE);
    setContentView(R.layout.activity_main);

    PD = new ProgressDialog(this);
    PD.setMessage("Loading...");
    PD.setCancelable(true);
    PD.setCanceledOnTouchOutside(false);
    auth = FirebaseAuth.getInstance();

    inputEmail =  (EditText) findViewById(R.id.email);
    inputPassword =  (EditText) findViewById(R.id.password);
    btnSignUp =  (Button) findViewById(R.id.sign_up_button);
    btnLogin = (Button) findViewById(R.id.sign_in_button);

    btnLogin.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        final String email = inputEmail.getText().toString();
        final String password = inputPassword.getText().toString();

        try {

          if(email.isEmpty()){
              inputEmail.setError("Email required");
              inputEmail.requestFocus();
              return;
          }

          if (password.isEmpty()){
              inputPassword.setError("Password required");
              inputPassword.requestFocus();
              return;
          }
            if (password.length() < 6) {
                inputPassword.setError("Password should be greater that 6 characters");
                inputPassword.requestFocus();
                return;
            }

            PD.show();
            auth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {
                      @Override
                      public void onComplete(@NonNull Task<AuthResult> task) {
                        if (!task.isSuccessful()) {
                          Toast.makeText(
                                  MainActivity.this,
                                  "Authentication Failed",
                                  Toast.LENGTH_LONG).show();
                          Log.v("error", task.getResult().toString());
                        } else {
                          Intent intent = new Intent(MainActivity.this, HomePage2Activity.class);
                          startActivity(intent);
                          finish();
                        }
                        PD.dismiss();
                      }
                    });

        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    });

    btnSignUp.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent intent = new Intent(MainActivity.this, RegisterActivity.class);
        startActivity(intent);
      }
    });

  }

  @Override
  protected void onResume() {
    if (auth.getCurrentUser() != null) {
      startActivity(new Intent(MainActivity.this, HomePage2Activity.class));
      finish();
    }
    super.onResume();
  }
}

